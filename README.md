# Configs for BitBucket Pipelines #

Some config samples. All variables are stored in 
[Environment variables](https://confluence.atlassian.com/bitbucket/environment-variables-794502608.html).

### Deploy via FTP ###

Using default Atlassian docker image and NcFTP. **$FTP_REMOTE_PATH** should be an absolute path, like **/www/zzz**. 
**$BITBUCKET_CLONE_DIR** is automatically set by Pipelines, so you only need to set FTP variables.

```
#!shell
pipelines:
  branches:
    master:
      - step:
          script:
            - export DEBIAN_FRONTEND=noninteractive
            - apt-get update
            - apt-get -qq install ncftp
            - ncftpput -v -u "$FTP_USERNAME" -p "$FTP_PASSWORD" -R $FTP_HOST $FTP_REMOTE_PATH $BITBUCKET_CLONE_DIR/*
```

### Deploy via FTP on demand ###

How to [Run pipelines manually](https://confluence.atlassian.com/bitbucket/run-pipelines-manually-861242583.html).
It's necessary for config to contain at least one existing branch path. Otherwise it will not pass validation.

```
#!shell
pipelines:
  custom:
    deploy-on-demand:
      - step:
          script:
            - export DEBIAN_FRONTEND=noninteractive
            - apt-get update
            - apt-get -qq install ncftp
            - ncftpput -v -u "$FTP_USERNAME" -p "$FTP_PASSWORD" -R $FTP_HOST $FTP_REMOTE_PATH $BITBUCKET_CLONE_DIR/*
  branches:
    master:
      - step:
          script:
            - echo "Do nothing here."
```